﻿namespace Grain_Growth_2D
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.PlayPauseButton = new System.Windows.Forms.Button();
            this.NextStepButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SizeOfTheSpace = new System.Windows.Forms.TextBox();
            this.NumberOfGrainSeeds = new System.Windows.Forms.TextBox();
            this.Neighbourhood = new System.Windows.Forms.ComboBox();
            this.BoundaryCondition = new System.Windows.Forms.ComboBox();
            this.ImportButton = new System.Windows.Forms.Button();
            this.ExportButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.NumberOfInclusions = new System.Windows.Forms.TextBox();
            this.MinRadius = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.MaxRadius = new System.Windows.Forms.TextBox();
            this.InclusionsButton = new System.Windows.Forms.Button();
            this.GBCButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.XValue = new System.Windows.Forms.TextBox();
            this.SecondPhaseButton = new System.Windows.Forms.Button();
            this.BoundaryButton = new System.Windows.Forms.Button();
            this.BoundLength = new System.Windows.Forms.Label();
            this.AverageGrainArea = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(373, 26);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(400, 400);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            this.pictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox_MouseClick);
            // 
            // PlayPauseButton
            // 
            this.PlayPauseButton.Location = new System.Drawing.Point(35, 26);
            this.PlayPauseButton.Name = "PlayPauseButton";
            this.PlayPauseButton.Size = new System.Drawing.Size(75, 23);
            this.PlayPauseButton.TabIndex = 1;
            this.PlayPauseButton.Text = "Play";
            this.PlayPauseButton.UseVisualStyleBackColor = true;
            this.PlayPauseButton.Click += new System.EventHandler(this.PlayPauseButton_Click);
            // 
            // NextStepButton
            // 
            this.NextStepButton.Location = new System.Drawing.Point(144, 26);
            this.NextStepButton.Name = "NextStepButton";
            this.NextStepButton.Size = new System.Drawing.Size(75, 23);
            this.NextStepButton.TabIndex = 2;
            this.NextStepButton.Text = "Next Step";
            this.NextStepButton.UseVisualStyleBackColor = true;
            this.NextStepButton.Click += new System.EventHandler(this.NextStepButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Location = new System.Drawing.Point(260, 26);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(75, 23);
            this.ResetButton.TabIndex = 3;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Size of the space";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Number of grain seeds";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 112);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Neighbourhood";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Boundary Condition";
            // 
            // SizeOfTheSpace
            // 
            this.SizeOfTheSpace.Location = new System.Drawing.Point(164, 68);
            this.SizeOfTheSpace.Name = "SizeOfTheSpace";
            this.SizeOfTheSpace.Size = new System.Drawing.Size(100, 20);
            this.SizeOfTheSpace.TabIndex = 8;
            this.SizeOfTheSpace.TextChanged += new System.EventHandler(this.SizeOfTheSpace_TextChanged);
            // 
            // NumberOfGrainSeeds
            // 
            this.NumberOfGrainSeeds.Location = new System.Drawing.Point(164, 90);
            this.NumberOfGrainSeeds.Name = "NumberOfGrainSeeds";
            this.NumberOfGrainSeeds.Size = new System.Drawing.Size(100, 20);
            this.NumberOfGrainSeeds.TabIndex = 9;
            this.NumberOfGrainSeeds.TextChanged += new System.EventHandler(this.NumberOfGrainSeeds_TextChanged);
            // 
            // Neighbourhood
            // 
            this.Neighbourhood.FormattingEnabled = true;
            this.Neighbourhood.Items.AddRange(new object[] {
            "von Neumanna",
            "Moore\'a",
            "Hexagonal left",
            "Hexagonal right",
            "Hexagonal random",
            "Pentagonal left",
            "Pentagonal right",
            "Pentagonal random"});
            this.Neighbourhood.Location = new System.Drawing.Point(164, 112);
            this.Neighbourhood.Name = "Neighbourhood";
            this.Neighbourhood.Size = new System.Drawing.Size(171, 21);
            this.Neighbourhood.TabIndex = 10;
            this.Neighbourhood.SelectedIndexChanged += new System.EventHandler(this.Neighbourhood_SelectedIndexChanged);
            // 
            // BoundaryCondition
            // 
            this.BoundaryCondition.FormattingEnabled = true;
            this.BoundaryCondition.Items.AddRange(new object[] {
            "Periodic boundary condition",
            "Non-periodic boundary condition"});
            this.BoundaryCondition.Location = new System.Drawing.Point(164, 134);
            this.BoundaryCondition.Name = "BoundaryCondition";
            this.BoundaryCondition.Size = new System.Drawing.Size(171, 21);
            this.BoundaryCondition.TabIndex = 11;
            this.BoundaryCondition.SelectedIndexChanged += new System.EventHandler(this.BoundaryCondition_SelectedIndexChanged);
            // 
            // ImportButton
            // 
            this.ImportButton.Location = new System.Drawing.Point(35, 403);
            this.ImportButton.Name = "ImportButton";
            this.ImportButton.Size = new System.Drawing.Size(75, 23);
            this.ImportButton.TabIndex = 12;
            this.ImportButton.Text = "Import";
            this.ImportButton.UseVisualStyleBackColor = true;
            this.ImportButton.Click += new System.EventHandler(this.ImportButton_Click);
            // 
            // ExportButton
            // 
            this.ExportButton.Location = new System.Drawing.Point(144, 403);
            this.ExportButton.Name = "ExportButton";
            this.ExportButton.Size = new System.Drawing.Size(75, 23);
            this.ExportButton.TabIndex = 13;
            this.ExportButton.Text = "Export";
            this.ExportButton.UseVisualStyleBackColor = true;
            this.ExportButton.Click += new System.EventHandler(this.ExportButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 182);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Number of inclusions";
            // 
            // NumberOfInclusions
            // 
            this.NumberOfInclusions.Location = new System.Drawing.Point(164, 179);
            this.NumberOfInclusions.Name = "NumberOfInclusions";
            this.NumberOfInclusions.Size = new System.Drawing.Size(62, 20);
            this.NumberOfInclusions.TabIndex = 15;
            this.NumberOfInclusions.TextChanged += new System.EventHandler(this.NumberOfInclusions_TextChanged);
            // 
            // MinRadius
            // 
            this.MinRadius.Location = new System.Drawing.Point(99, 208);
            this.MinRadius.Name = "MinRadius";
            this.MinRadius.Size = new System.Drawing.Size(44, 20);
            this.MinRadius.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 211);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Min radius";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(180, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Max radius";
            // 
            // MaxRadius
            // 
            this.MaxRadius.Location = new System.Drawing.Point(241, 208);
            this.MaxRadius.Name = "MaxRadius";
            this.MaxRadius.Size = new System.Drawing.Size(44, 20);
            this.MaxRadius.TabIndex = 18;
            // 
            // InclusionsButton
            // 
            this.InclusionsButton.Location = new System.Drawing.Point(260, 177);
            this.InclusionsButton.Name = "InclusionsButton";
            this.InclusionsButton.Size = new System.Drawing.Size(75, 23);
            this.InclusionsButton.TabIndex = 20;
            this.InclusionsButton.Text = "Inclusions";
            this.InclusionsButton.UseVisualStyleBackColor = true;
            this.InclusionsButton.Click += new System.EventHandler(this.InclusionsButton_Click);
            // 
            // GBCButton
            // 
            this.GBCButton.Location = new System.Drawing.Point(35, 250);
            this.GBCButton.Name = "GBCButton";
            this.GBCButton.Size = new System.Drawing.Size(75, 23);
            this.GBCButton.TabIndex = 21;
            this.GBCButton.Text = "GBC ON";
            this.GBCButton.UseVisualStyleBackColor = true;
            this.GBCButton.Click += new System.EventHandler(this.GBCButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(127, 255);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(122, 13);
            this.label8.TabIndex = 22;
            this.label8.Text = "Probability of change (X)";
            // 
            // XValue
            // 
            this.XValue.Location = new System.Drawing.Point(260, 252);
            this.XValue.Name = "XValue";
            this.XValue.Size = new System.Drawing.Size(75, 20);
            this.XValue.TabIndex = 23;
            this.XValue.TextChanged += new System.EventHandler(this.XValue_TextChanged);
            // 
            // SecondPhaseButton
            // 
            this.SecondPhaseButton.Location = new System.Drawing.Point(35, 299);
            this.SecondPhaseButton.Name = "SecondPhaseButton";
            this.SecondPhaseButton.Size = new System.Drawing.Size(75, 23);
            this.SecondPhaseButton.TabIndex = 24;
            this.SecondPhaseButton.Text = "2nd Phase";
            this.SecondPhaseButton.UseVisualStyleBackColor = true;
            this.SecondPhaseButton.Click += new System.EventHandler(this.SecondPhaseButton_Click);
            // 
            // BoundaryButton
            // 
            this.BoundaryButton.Location = new System.Drawing.Point(144, 299);
            this.BoundaryButton.Name = "BoundaryButton";
            this.BoundaryButton.Size = new System.Drawing.Size(75, 23);
            this.BoundaryButton.TabIndex = 25;
            this.BoundaryButton.Text = "Bound";
            this.BoundaryButton.UseVisualStyleBackColor = true;
            this.BoundaryButton.Click += new System.EventHandler(this.BoundaryButton_Click);
            // 
            // BoundLength
            // 
            this.BoundLength.AutoSize = true;
            this.BoundLength.Location = new System.Drawing.Point(38, 340);
            this.BoundLength.Name = "BoundLength";
            this.BoundLength.Size = new System.Drawing.Size(0, 13);
            this.BoundLength.TabIndex = 26;
            // 
            // AverageGrainArea
            // 
            this.AverageGrainArea.AutoSize = true;
            this.AverageGrainArea.Location = new System.Drawing.Point(38, 367);
            this.AverageGrainArea.Name = "AverageGrainArea";
            this.AverageGrainArea.Size = new System.Drawing.Size(0, 13);
            this.AverageGrainArea.TabIndex = 27;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.AverageGrainArea);
            this.Controls.Add(this.BoundLength);
            this.Controls.Add(this.BoundaryButton);
            this.Controls.Add(this.SecondPhaseButton);
            this.Controls.Add(this.XValue);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.GBCButton);
            this.Controls.Add(this.InclusionsButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.MaxRadius);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.MinRadius);
            this.Controls.Add(this.NumberOfInclusions);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ExportButton);
            this.Controls.Add(this.ImportButton);
            this.Controls.Add(this.BoundaryCondition);
            this.Controls.Add(this.Neighbourhood);
            this.Controls.Add(this.NumberOfGrainSeeds);
            this.Controls.Add(this.SizeOfTheSpace);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.NextStepButton);
            this.Controls.Add(this.PlayPauseButton);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.Text = "Grain Growth 2D";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button PlayPauseButton;
        private System.Windows.Forms.Button NextStepButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox SizeOfTheSpace;
        private System.Windows.Forms.TextBox NumberOfGrainSeeds;
        private System.Windows.Forms.ComboBox Neighbourhood;
        private System.Windows.Forms.ComboBox BoundaryCondition;
        private System.Windows.Forms.Button ImportButton;
        private System.Windows.Forms.Button ExportButton;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NumberOfInclusions;
        private System.Windows.Forms.TextBox MinRadius;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox MaxRadius;
        private System.Windows.Forms.Button InclusionsButton;
        private System.Windows.Forms.Button GBCButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox XValue;
        private System.Windows.Forms.Button SecondPhaseButton;
        private System.Windows.Forms.Button BoundaryButton;
        private System.Windows.Forms.Label BoundLength;
        private System.Windows.Forms.Label AverageGrainArea;
    }
}


﻿using OpenTK;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Grain_Growth_2D
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            space.ResetSpace();
            space.ResetInclusions();
            pictureBox.Image = space.GenerateImage();
        }

        Space space = new Space();
        bool deploy = false, play = false;
        Vector2[] NeighbourhoodArray;

        #region Standard Field
        private void SizeOfTheSpace_TextChanged(object sender, EventArgs e)
        {
            if (SizeOfTheSpace.Text != "")
                space.sizeOfSpace = int.Parse(SizeOfTheSpace.Text);
        }

        private void NumberOfGrainSeeds_TextChanged(object sender, EventArgs e)
        {
            if (NumberOfGrainSeeds.Text != "")
                space.numberOfSeeds = int.Parse(NumberOfGrainSeeds.Text);
        }

        private void Neighbourhood_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Neighbourhood.Text == "von Neumanna")
            {
                NeighbourhoodArray = new Vector2[4];
                NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);
                NeighbourhoodArray.SetValue(new Vector2(1, 0), 1);
                NeighbourhoodArray.SetValue(new Vector2(0, -1), 2);
                NeighbourhoodArray.SetValue(new Vector2(-1, 0), 3);
            }

            if (Neighbourhood.Text == "Moore'a")
            {
                NeighbourhoodArray = new Vector2[8];
                NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);//
                NeighbourhoodArray.SetValue(new Vector2(1, 1), 1);
                NeighbourhoodArray.SetValue(new Vector2(1, 0), 2);//
                NeighbourhoodArray.SetValue(new Vector2(1, -1), 3);
                NeighbourhoodArray.SetValue(new Vector2(0, -1), 4);//
                NeighbourhoodArray.SetValue(new Vector2(-1, -1), 5);
                NeighbourhoodArray.SetValue(new Vector2(-1, 0), 6);//
                NeighbourhoodArray.SetValue(new Vector2(-1, 1), 7);
            }

            if (Neighbourhood.Text == "Hexagonal left"|Neighbourhood.Text == "Hexagonal random")
            {
                NeighbourhoodArray = new Vector2[6];
                NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);
                NeighbourhoodArray.SetValue(new Vector2(1, 1), 1);
                NeighbourhoodArray.SetValue(new Vector2(1, 0), 2);
                NeighbourhoodArray.SetValue(new Vector2(0, -1), 3);
                NeighbourhoodArray.SetValue(new Vector2(-1, -1), 4);
                NeighbourhoodArray.SetValue(new Vector2(-1, 0), 5);
            }

            if (Neighbourhood.Text == "Hexagonal right")
            {
                NeighbourhoodArray = new Vector2[6];
                NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);
                NeighbourhoodArray.SetValue(new Vector2(1, 0), 1);
                NeighbourhoodArray.SetValue(new Vector2(1, -1), 2);
                NeighbourhoodArray.SetValue(new Vector2(0, -1), 3);
                NeighbourhoodArray.SetValue(new Vector2(-1, 0), 4);
                NeighbourhoodArray.SetValue(new Vector2(-1, 1), 5);
            }

            if(Neighbourhood.Text == "Pentagonal right" | Neighbourhood.Text == "Pentagonal random")
            {
                NeighbourhoodArray = new Vector2[5];
                NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);
                NeighbourhoodArray.SetValue(new Vector2(0, -1), 1);
                NeighbourhoodArray.SetValue(new Vector2(-1, -1), 2);
                NeighbourhoodArray.SetValue(new Vector2(-1, 0), 3);
                NeighbourhoodArray.SetValue(new Vector2(-1, 1), 4);
            }

            if(Neighbourhood.Text == "Pentagonal left")
            {
                NeighbourhoodArray = new Vector2[5];
                NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);
                NeighbourhoodArray.SetValue(new Vector2(1, 1), 1);
                NeighbourhoodArray.SetValue(new Vector2(1, 0), 2);
                NeighbourhoodArray.SetValue(new Vector2(1, -1), 3);
                NeighbourhoodArray.SetValue(new Vector2(0, -1), 4);
            }
        }

        private void BoundaryCondition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (BoundaryCondition.Text == "Non-periodic boundary condition")
                space.bound = true;
            else
                space.bound = false;
        }
        #endregion

        #region Standard Buttons
        private void PlayPauseButton_Click(object sender, EventArgs e)
        {
            play = !play;

            if(play==true&&space.zeros==true)
            {
                PlayPauseButton.Text = "Pause";

                if (deploy == false)
                {
                    pictureBox.Image = space.DeploySeeds(0);
                    deploy = true;
                }

                Thread playThread = new Thread(new ThreadStart(Play));
                playThread.Start();
            }
            else
                PlayPauseButton.Text = "Play";
        }

        private void Play()
        {
            while(play==true & space.zeros==true)
            {
                space.NextStep(NeighbourhoodArray);
                pictureBox.Image = space.GenerateImage();
            }
        }

        private void NextStepButton_Click(object sender, EventArgs e)
        {
            if(!play)
            {
                if (deploy == false)
                {
                    pictureBox.Image = space.DeploySeeds(0);
                    deploy = true;
                }
                else
                {
                    for (int i = 0; i < 20; i++)
                        space.NextStep(NeighbourhoodArray);
                    pictureBox.Image = space.GenerateImage();
                }
            }   
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            if(!play)
            {
                space.ResetSpace();
                space.ResetInclusions();

                pictureBox.Image = space.GenerateImage();
                PlayPauseButton.Text = "Play";

                deploy = false;
            }            
        }
        #endregion

        #region Import/Export Buttons
        private void ImportButton_Click(object sender, EventArgs e)
        {
            if(!play)
            {
                space.ResetSpace();
                deploy = true;
                var reader = new StreamReader("data.csv");

                SizeOfTheSpace.Text = reader.ReadLine();
                NumberOfGrainSeeds.Text = reader.ReadLine();
                Neighbourhood.Text = reader.ReadLine();
                BoundaryCondition.Text = reader.ReadLine();

                int[][] Array = new int[int.Parse(SizeOfTheSpace.Text) + 2][];
                int[][] InclusionsArray = new int[int.Parse(SizeOfTheSpace.Text)][];

                for (int i = 0; i < int.Parse(SizeOfTheSpace.Text) + 2; i++)
                {
                    Array[i] = new int[int.Parse(SizeOfTheSpace.Text) + 2];
                    var line = reader.ReadLine();
                    var chars = line.Split(',');

                    for (int j = 0; j < int.Parse(SizeOfTheSpace.Text) + 2; j++)
                        Array[i][j] = int.Parse(chars[j]);
                }

                if (!reader.EndOfStream)
                {
                    NumberOfInclusions.Text = reader.ReadLine();
                    MinRadius.Text = reader.ReadLine();
                    MaxRadius.Text = reader.ReadLine();

                    for (int i = 0; i < int.Parse(SizeOfTheSpace.Text); i++)
                    {
                        InclusionsArray[i] = new int[int.Parse(SizeOfTheSpace.Text)];
                        var line = reader.ReadLine();
                        var chars = line.Split(',');

                        for (int j = 0; j < int.Parse(SizeOfTheSpace.Text); j++)
                            InclusionsArray[i][j] = int.Parse(chars[j]);
                    }
                    space.SetInclusionsArray(InclusionsArray);
                }

                reader.Close();
                space.SetArray(Array);
                pictureBox.Image = space.GenerateImage();
            }
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            StringBuilder csv = new StringBuilder();
            int[][] Array = space.GetArray();
            int[][] InclusionsArray = space.GetInclusionsArray();

            csv.AppendLine(SizeOfTheSpace.Text);
            csv.AppendLine(NumberOfGrainSeeds.Text);
            csv.AppendLine(Neighbourhood.Text);
            csv.AppendLine(BoundaryCondition.Text);

            for (int y = 0; y < int.Parse(SizeOfTheSpace.Text) + 2; y++)
            {
                for (int x = 0; x < int.Parse(SizeOfTheSpace.Text) + 2; x++)
                    csv.Append(Array[x][y] + ",");
                csv.AppendLine();
            }

            if (NumberOfInclusions.Text != "")
            {
                csv.AppendLine(NumberOfInclusions.Text);
                csv.AppendLine(MinRadius.Text);
                csv.AppendLine(MaxRadius.Text);

                for (int y = 0; y < int.Parse(SizeOfTheSpace.Text); y++)
                {
                    for (int x = 0; x < int.Parse(SizeOfTheSpace.Text); x++)
                        csv.Append(InclusionsArray[x][y] + ",");
                    csv.AppendLine();
                }
            }

            File.WriteAllText("data.csv", csv.ToString());
            space.GenerateImage().Save("image.bmp");
        }
        #endregion

        #region Inclusions
        private void InclusionsButton_Click(object sender, EventArgs e)
        {
            space.ResetInclusions();
            space.GenerateInclusions(int.Parse(NumberOfInclusions.Text), int.Parse(MinRadius.Text),
                int.Parse(MaxRadius.Text));

            pictureBox.Image = space.GenerateImage();
        }

        private void NumberOfInclusions_TextChanged(object sender, EventArgs e)
        {
            if (NumberOfInclusions.Text == "")
                space.inclusions = false;
        }
        #endregion

        #region GBC
        private void GBCButton_Click(object sender, EventArgs e)
        {
            if(!play)
            {
                space.gbc = !space.gbc;

                if (space.gbc == true)
                {
                    GBCButton.Text = "GBC OFF";
                    Neighbourhood.Text = "Moore'a";
                    Neighbourhood.Enabled = false;

                    NeighbourhoodArray = new Vector2[8];
                    NeighbourhoodArray.SetValue(new Vector2(0, 1), 0);//
                    NeighbourhoodArray.SetValue(new Vector2(1, 1), 1);
                    NeighbourhoodArray.SetValue(new Vector2(1, 0), 2);//
                    NeighbourhoodArray.SetValue(new Vector2(1, -1), 3);
                    NeighbourhoodArray.SetValue(new Vector2(0, -1), 4);//
                    NeighbourhoodArray.SetValue(new Vector2(-1, -1), 5);
                    NeighbourhoodArray.SetValue(new Vector2(-1, 0), 6);//
                    NeighbourhoodArray.SetValue(new Vector2(-1, 1), 7);

                }
                else
                {
                    GBCButton.Text = "GBC ON";
                    Neighbourhood.Enabled = true;
                }
            }  
        }

        private void XValue_TextChanged(object sender, EventArgs e)
        {
            if(XValue.Text != "")
                space.xValue = int.Parse(XValue.Text);

        }
        #endregion

        #region Second Phase
        private void SecondPhaseButton_Click(object sender, EventArgs e)
        {
            if(!play)
                space.StartSecondPhase();
        }

        private void pictureBox_MouseClick(object sender, MouseEventArgs e)
        {            
            pictureBox.Image = space.DeleteGrain(e.X, e.Y);
        }
        #endregion

        #region Bound
        private void BoundaryButton_Click(object sender, EventArgs e)
        {
            pictureBox.Image = space.DrawBound();
            AverageGrainArea.Text = "Average grain area: " + space.CalculateAverageGrainArea().ToString();
            BoundLength.Text = "Bound length: " + space.CalculateBoundLength().ToString();
        }
        #endregion        
    }
}

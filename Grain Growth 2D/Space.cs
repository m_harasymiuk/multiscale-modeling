﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Grain_Growth_2D
{
    public class Space
    {
        #region Declarations
        public int sizeOfSpace = 500, numberOfSeeds = 0, xValue = 100, firstPhaseMaxGrainId = 0;
        int sizeOfArrays;

        int[][] NowArray, NextArray, InclusionsArray;

        List<int> NeighbourhoodValues = new List<int>();
        List<int> NeighbourhoodValuesWithoutZeros = new List<int>();
        List<int> NeighbourhoodMaxValue = new List<int>();
        List<int[]> NeighbourhoodValuesWithCounter = new List<int[]>(); //[0] values, [1] counter
        List <Color> Colors = new List<Color>();

        public bool bound, zeros = true, inclusions = false, gbc=false, secondPhase=false;

        public Bitmap bitmap;

        Random random = new Random();
        #endregion

        public void ResetSpace()
        {
            sizeOfArrays = sizeOfSpace + 2;
            zeros = true;
            secondPhase = false;
            firstPhaseMaxGrainId = 0;

            NowArray = new int[sizeOfArrays][];
            NextArray = new int[sizeOfArrays][];            

            bitmap = new Bitmap(sizeOfSpace, sizeOfSpace);

            for (int i = 0; i < sizeOfArrays; i++)
            {
                NowArray[i] = new int[sizeOfArrays];
                NextArray[i] = new int[sizeOfArrays];
            }

            GenerateColors(0);
        }

        #region GenerateColors
        public void GenerateColors(int startNumber)
        {
            if (!secondPhase)
            {
                Colors.Clear();
                Colors.Add(Color.White);
            }             
            
            for (int i = startNumber+1; i <= startNumber+numberOfSeeds; i++)
                Colors.Add(GenerateRandomColor());
        }

        Color GenerateRandomColor()
        {
            return Color.FromArgb(random.Next(10, 245), random.Next(10, 245), random.Next(10, 245));
        }
        #endregion

        public Bitmap GenerateImage()
        {
            Bitmap temp = new Bitmap(bitmap);

            for (int y = 0; y < sizeOfSpace; y++)
                for (int x = 0; x < sizeOfSpace; x++)
                    if (InclusionsArray[x][y] == -1 || NowArray[x+1][y+1]==-1)
                        temp.SetPixel(x, y, Color.Black);
                    else
                        temp.SetPixel(x, y, Colors[NowArray[x + 1][y + 1]]);

            return temp;
        }

        public Bitmap DeploySeeds(int startNumber)
        {
            if(!secondPhase)
                ResetSpace();

            Random random = new Random();
            int x, y;

            for (int grainId = startNumber+1; grainId <= startNumber+numberOfSeeds; grainId++)
            {
                x = random.Next(sizeOfSpace);
                y = random.Next(sizeOfSpace);

                if(inclusions || secondPhase) 
                {
                    if(InclusionsArray[x][y]==-1 || NowArray[x][y]!=0)
                    {
                        grainId--;
                        continue;
                    }
                }

                NowArray[x + 1][y + 1] = grainId;
                NextArray[x + 1][y + 1] = grainId;
            }

            return GenerateImage();
        }

        public void NextStep(Vector2[] NeighbourhoodArray)
        {
            Random random = new Random();
            if (!bound)
                SetBound();

            zeros = false;

            for (int y = 1; y < sizeOfArrays - 1; y++)
            {
                for (int x = 1; x < sizeOfArrays - 1; x++)
                {
                    if ((NowArray[x][y] != 0)||(InclusionsArray[x-1][y-1]==-1))
                        continue;

                    zeros = true;
                    NeighbourhoodValues.Clear();
                    NeighbourhoodValuesWithoutZeros.Clear();
                    NeighbourhoodValuesWithCounter.Clear();

                    foreach (Vector2 vector in NeighbourhoodArray)
                    {
                        NeighbourhoodValues.Add(NowArray[x + (int)vector[0]][y + (int)vector[1]]);
                    }

                    NeighbourhoodValuesWithoutZeros = NeighbourhoodValues.FindAll(value => value >firstPhaseMaxGrainId);

                    if (NeighbourhoodValuesWithoutZeros.Count == 0)
                        continue;

                    NeighbourhoodValues.Sort();
                    NeighbourhoodValuesWithCounter.Add(new int[2] { NeighbourhoodValuesWithoutZeros[0], 1 });

                    for (int i = 1; i < NeighbourhoodValuesWithoutZeros.Count; i++)
                    {
                        if (NeighbourhoodValuesWithoutZeros[i] == NeighbourhoodValuesWithoutZeros[i - 1])
                        {
                            NeighbourhoodValuesWithCounter[NeighbourhoodValuesWithCounter.Count - 1][1]++;
                        }
                        else
                            NeighbourhoodValuesWithCounter.Add(new int[2] { NeighbourhoodValuesWithoutZeros[i], 1 });
                    }

                    if(gbc == false)
                    {
                        NextArray[x][y] = CheckRule4(NeighbourhoodValuesWithCounter);
                    }
                    else
                    {
                        NextArray[x][y] = CheckRule1(NeighbourhoodValues);

                        if (NextArray[x][y] != 0)
                            continue;

                        NextArray[x][y] = CheckRule2(NeighbourhoodValues);

                        if (NextArray[x][y] != 0)
                            continue;

                        NextArray[x][y] = CheckRule3(NeighbourhoodValues);

                        if (NextArray[x][y] != 0)
                            continue;
                       
                        int probability = random.Next(0, 101);

                        if (probability <= xValue)
                            NextArray[x][y] = CheckRule4(NeighbourhoodValuesWithCounter);
                        else
                            NextArray[x][y] = 0;
                    }                    
                }
            }

            CopyArray(NextArray, NowArray);
        }

        #region Rules
        public int CheckRule1(List<int> Values)
        {                   
            foreach(int value in Values)
            {
                if(Values.FindAll(v => v == value).Count >= 5 && value>firstPhaseMaxGrainId)
                {
                    return value;
                }
            }
            return 0;
        }

        public int CheckRule2(List<int> Values)
        {
            List<int> NewValues = new List<int>
            {
                Values[2],
                Values[4],
                Values[6],
                Values[0]
            };

            NewValues = NewValues.FindAll(value => value > firstPhaseMaxGrainId);

            if (NewValues.Count < 3)
                return 0;

            foreach(int value in NewValues)
            {
                if (NewValues.FindAll(v => v == value).Count >= 3)
                {
                    return value;
                }                    
            }

            return 0;
        }

        public int CheckRule3(List<int> Values)
        {
            List<int> NewValues = new List<int>
            {
                Values[1],
                Values[3],
                Values[5],
                Values[7]
            };

            NewValues = NewValues.FindAll(value => value >firstPhaseMaxGrainId);

            if (NewValues.Count < 3)
                return 0;

            foreach (int value in NewValues)
            {
                if (NewValues.FindAll(v => v == value).Count >= 3)
                {
                    return value;
                }                    
            }

            return 0;
        }

        public int CheckRule4(List<int[]> ValuesWithCounter)
        {
            if (ValuesWithCounter.Count == 1)
            {
                return ValuesWithCounter[0][0];
            }

            NeighbourhoodMaxValue.Clear();
            int max = 0;

            foreach (int[] value in ValuesWithCounter)
            {
                if (value[1] > max)
                {
                    NeighbourhoodMaxValue.Clear();
                    max = value[1];
                    NeighbourhoodMaxValue.Add(value[0]);
                }
                else if (value[1] == max)
                    NeighbourhoodMaxValue.Add(value[0]);
            }

            if (NeighbourhoodMaxValue.Count == 1)
            {
                return NeighbourhoodMaxValue[0];
            }

            Random random = new Random();
            return NeighbourhoodMaxValue[random.Next(NeighbourhoodMaxValue.Count)];
        }
        #endregion

        public void SetBound()
        {
            for (int x = 1; x < sizeOfArrays - 1; x++)
            {
                NowArray[x][0] = NowArray[x][sizeOfSpace];
                NowArray[x][sizeOfArrays - 1] = NowArray[x][1];
            }

            for (int y = 0; y < sizeOfArrays; y++)
            {
                NowArray[0][y] = NowArray[sizeOfSpace][y];
                NowArray[sizeOfArrays - 1][y] = NowArray[1][y];
            }
        }

        #region ArrayOperations
        public int[][] GetArray()
        {
            return NowArray;
        }

        public void SetArray(int[][] NewArray)
        {
            ResetSpace();

            for (int i = 0; i < sizeOfArrays; i++)
            {
                Array.Copy(NewArray[i], NowArray[i], sizeOfArrays);
                Array.Copy(NewArray[i], NextArray[i], sizeOfArrays);
            }

        }

        public void CopyArray(int[][] SourceArray, int[][] DestinationArray)
        {
            for (int i = 0; i < sizeOfArrays; i++)
                Array.Copy(SourceArray[i], DestinationArray[i], sizeOfArrays);
        }
        #endregion

        #region Inclusions
        public int[][] GetInclusionsArray()
        {
            return InclusionsArray;
        }

        public void SetInclusionsArray(int[][] NewArray)
        {
            for (int i = 0; i < sizeOfSpace; i++)
            {
                Array.Copy(NewArray[i], InclusionsArray[i], sizeOfSpace);                
            }

        }

        public void GenerateInclusions(int number, int minRadius, int maxRadius)
        {
            ResetInclusions();
            inclusions = true;

            Random random = new Random();
            int x, y, r;

            for (int i = 0; i < number; i++)
            {
                x = random.Next(sizeOfSpace);
                y = random.Next(sizeOfSpace);
                r = random.Next(minRadius, maxRadius);
                FillTheCircle(y, x, r);
            }
        }

        public void ResetInclusions()
        {
            InclusionsArray = new int[sizeOfSpace][];
            for (int i = 0; i < sizeOfSpace; i++)
                InclusionsArray[i] = new int[sizeOfSpace];
        }

        public void FillTheCircle(int y, int x, int r)
        {
            for (int i = y - r; i <= y + r; i++)
            {
                for (int j = x - r; j <= x + r; j++)
                {
                    if (IsInTheCircle(Math.Abs(y - i),
                        Math.Abs(x - j), r) & sizeOfSpace > i & i >= 0 & j >= 0 & sizeOfSpace > j)
                    {
                        InclusionsArray[i][j] = -1;
                    }
                }
            }
        }

        public bool IsInTheCircle(int x, int y, int r)
        {
            return (x * x) + y * y <= r * r;
        }
        #endregion
       
        #region DualPhase
        public Bitmap DeleteGrain(int x, int y)
        {
            int grainId = NowArray[x + 1][y + 1];
            zeros = true;            
            return RegenerateImage(grainId);
        }

        public Bitmap RegenerateImage(int grainId)
        {
            for(int y=0; y<sizeOfArrays; y++)
            {
                for (int x = 0; x < sizeOfArrays; x++)
                {
                    if (NowArray[x][y] == grainId)
                        NowArray[x][y] = 0;
                }
            }
            CopyArray(NowArray, NextArray);
            return GenerateImage();
        }

        public void StartSecondPhase()
        {
            secondPhase = true;
            firstPhaseMaxGrainId = FindMaxGrainId();
            GenerateColors(firstPhaseMaxGrainId);
            DeploySeeds(firstPhaseMaxGrainId);
        }

        private int FindMaxGrainId()
        {
            int result=0;

            for (int y = 0; y < sizeOfArrays; y++)
                for (int x = 0; x < sizeOfArrays; x++)
                    result = (NowArray[x][y] > result) ? NowArray[x][y] : result;

            return result;
        }
        #endregion

        #region Bound
        internal Bitmap DrawBound()
        {
            for (int y = 1; y < sizeOfArrays - 1; y++)
            {
                for (int x = 1; x < sizeOfArrays - 1; x++)
                {
                    if (IsBoundary(x, y))
                        NextArray[x][y] = -1;
                }
            }
            CopyArray(NextArray, NowArray);

            return GenerateImage();
        }

        private bool IsBoundary(int x, int y)
        {
            for(int yy=y-1; yy<=y+1; yy++)
            {
                for(int xx=x-1; xx<=x+1; xx++)
                {
                    if (NowArray[x][y] != NowArray[xx][yy])
                        return true;
                }
            }
            return false;
        }

        public int CalculateAverageGrainArea()
        {
            return sizeOfSpace * sizeOfSpace / (numberOfSeeds+firstPhaseMaxGrainId);
        }

        public int CalculateBoundLength()
        {
            int result = 0;
            for (int y = 1; y < sizeOfArrays - 1; y++)
            {
                for (int x = 1; x < sizeOfArrays - 1; x++)
                {
                    if (NextArray[x][y] == -1)
                        result++;
                }
            }
            return result / 2;
        }
        #endregion
    }
}
